
module "db" {
    source = "terraform-aws-modules/rds/aws"

    identifier = "${var.env}-db"

    engine                = "postgres"
    engine_version        = "10.4"
    instance_class        = "db.t2.micro"
    allocated_storage        = 256
    name                     = "terraform-${var.env}-db"
    publicly_accessible      = true
    storage_encrypted        = false
    storage_type             = "gp2"
    username                 = "postgresterraform"
    password                 = "yesplease"

    port                     = 5432
    multi_az                 = "${var.rds-prod}"
    backup_retention_period  = 7   # in days
 #   iam_database_authentication_enabled = true

    vpc_security_group_ids = ["${module.rds-sg.this_security_group_id}"]

    maintenance_window = "Mon:00:00-Mon:03:00"
    backup_window      = "03:00-06:00"

    # Enhanced Monitoring - see example for details on how to create the role
    # by yourself, in case you don't want to create it automatically
    
    tags = {
        Owner       = "${var.OwnerTag}"
        Environment = "${var.env}"
    }

    # DB subnet group
    subnet_ids = ["${module.vpc.database_subnets}"]


    # Snapshot name upon DB deletion
    final_snapshot_identifier = "postgres-terraform-${var.env}"

    # Database Deletion Protection
    deletion_protection = true

}
