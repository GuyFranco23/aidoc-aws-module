

module "buckets" {
  source            = "Modules/buckets"
  bucket            = "${var.bucket}"
  env               = "${var.env}"
  region =  "${var.region}"
  OwnerTag = "${var.OwnerTag}"
}
 