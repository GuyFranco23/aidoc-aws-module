# webserver
module "webserver-asg" {
  source = "terraform-aws-modules/autoscaling/aws"

  name = "${var.env}-webserver-asg"

  # Launch configuration
  lc_name = "${var.env}-webserver-lc"

    image_id        = "${var.AMI-webserver["${var.region}"]}"
    instance_type   = "t2.micro"
    security_groups = ["${module.public-sg.this_security_group_id}"]
    key_name = "${var.key_name}"
    associate_public_ip_address = true
    iam_instance_profile = "webserverRole"
    user_data = <<HEREDOC
        #!/bin/bash
        docker run -d -p 80:80 394233314359.dkr.ecr.us-east-2.amazonaws.com/web-server:v2
        HEREDOC
    
    

  # Auto scaling group
  asg_name                  = "${var.env}-webserver-asg"
  vpc_zone_identifier       = ["${module.vpc.public_subnets}"]
  health_check_type         = "EC2"
  min_size                  = 1
  max_size                  = 2
  desired_capacity          = 1
  wait_for_capacity_timeout = 0

  tags = [
    {
      key                 = "Environment"
      value               = "${var.env}"
      propagate_at_launch = true
    },
    {
       key                 = "Name"
    value               = "${var.VPCName}-webserver-ScaleGroup"
      propagate_at_launch = true
    },
  ]
  # alb
  target_group_arns = "${module.alb.target_group_arns}"
}


# #################  analysis   ###############################
module "analysis-asg" {
  source = "terraform-aws-modules/autoscaling/aws"

  name = "${var.env}-analysis-asg"

  # Launch configuration
  lc_name = "${var.env}-analysis-lc"

    image_id        = "${var.AMI-analysis["${var.region}"]}"
    instance_type = "p2.xlarge"
    security_groups = ["${module.Analysis-sg.this_security_group_id}"]
    key_name = "${var.key_name}"
    iam_instance_profile = "analysisRole"
    user_data = <<HEREDOC
        #!/bin/bash
        docker run -d -p 80:80 394233314359.dkr.ecr.us-east-2.amazonaws.com/analysis-server:v3
        HEREDOC
    
    

  # Auto scaling group
  asg_name                  = "${var.env}-analysis-asg"
  vpc_zone_identifier       = ["${module.vpc.private_subnets}"]
  health_check_type         = "EC2"
  min_size                  = 1
  max_size                  = 2
  desired_capacity          = 1
  wait_for_capacity_timeout = 0

  tags = [
    {
      key                 = "Environment"
      value               = "${var.env}"
      propagate_at_launch = true
    },
    {
       key                 = "Name"
    value               = "${var.VPCName}-analysis-ScaleGroup"
      propagate_at_launch = true
    },
  ]
}