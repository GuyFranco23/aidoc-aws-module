variable "OwnerTag" {
		type = "string"
		default = "Terraform"
		description = "OwnerTag for all resources"
	}
	variable "VPCName" {
		type = "string"
		default = "Terraform-VPC"
		description = "The VPC name"
	}