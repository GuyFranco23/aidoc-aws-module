// Front load balancer

module "alb" {
  source                        = "terraform-aws-modules/alb/aws"
  load_balancer_name            = "terraform-${var.env}-alb"
  security_groups               = ["${module.load-balancer-SG.this_security_group_id}"]
 # log_bucket_name               = "logs-us-east-2-123456789012"
 # log_location_prefix           = "my-alb-logs"
  subnets                       = ["${module.vpc.public_subnets}"]
  tags                          = "${map("Environment", "${var.env}")}"
  vpc_id                        = "${module.vpc.vpc_id}"
 # https_listeners               = "${list(map("certificate_arn", "arn:aws:iam::123456789012:server-certificate/test_cert-123456789012", "port", 443))}"
 # https_listeners_count         = "1"
  http_tcp_listeners            = "${list(map("port", "80", "protocol", "HTTP"))}"
  http_tcp_listeners_count      = "1"
  target_groups                 = "${list(map("name", "terraform-${var.env}-tg", "backend_protocol", "HTTP", "backend_port", "80"))}"
  target_groups_count           = "1"
}
