
########################
#Variables
########################

	variable "OwnerTag" {
		type = "string"
		default = "Terraform"
		description = "OwnerTag for all resources"
	}
	variable "VPCName" {
		type = "string"
		default = "Terraform-VPC"
		description = "The VPC name"
	}
	variable "region"{
		type = "string"
	}
	
	variable "customer_gateway_IP"{
		type = "string"
		default = "62.219.12.14"
	}
    variable "key_name" {
       default = "aidoc-dev-eu"
        description = "the ssh key to use in the EC2 machines"
	}
	variable "env" {
		default = "dev"
		description = "the evnironment"
 	
	}
	variable "AMI-webserver" {
  		type = "map"
		default = {
			"eu-west-1" = "ami-05be8adb97d09acd8"
			"eu-west-2" = "AMIXXX"
		}
	}
	variable "AMI-analysis" {
  		type = "map"
		default = {
			"eu-west-1" = "ami-05be8adb97d09acd8"
			"eu-west-2" = "AMIXXX"
		}
	}
	variable "bucket" {
		type = "string"
		default = "terraform-aidoc-default-bucket"
		description = "the bucket name"

	}
	variable "queue_name" {
		type = "string"
		default = "terraform-aidoc-default-queue"
		description = "the queue name"
	}
	variable "rds-prod" {
		type = "string"
		default = "false"
		description = "is the rds multi AZ"
	}

    variable "access_key" {
        description = "the access_key of your AWS account"}
 variable "secret_key" {
        description = "the secret_key of your AWS account"
    }
    


		