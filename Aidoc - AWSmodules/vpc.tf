module "vpc" {
	source = "terraform-aws-modules/vpc/aws"

  name = "vpc-terraform"

  cidr = "10.10.0.0/16"

  azs                 = ["${var.region}a", "${var.region}b", "${var.region}c"]
  private_subnets     = ["10.0.2.0/23", "10.0.8.0/23", "10.0.10.0/23"]
  public_subnets      = ["10.0.0.0/23", "10.0.4.0/23", "10.0.6.0/23"]
  database_subnets    = ["10.10.21.0/24", "10.10.22.0/24", "10.10.23.0/24"]
  

  create_database_subnet_group = false
	map_public_ip_on_launch = true
  enable_nat_gateway = true
  single_nat_gateway = true

 # enable_s3_endpoint       = true
 # enable_dynamodb_endpoint = true

  enable_dhcp_options              = true
  #dhcp_options_domain_name         = "service.consul"
 # dhcp_options_domain_name_servers = ["127.0.0.1", "10.10.0.2"]

  tags = {
    Owner       = "${var.OwnerTag}"
    Environment = "${var.env}"
    Name        = "vpc-terraform"
  }
}
/*
####################  subnets  #######################################
module "public-subnet-a"{
    source = "/Modules/subnet-module"

    vpc_id = "${module.vpc.id}"
    cidr_block = "10.0.0.0/23"
    availability_zone = "${var.region}a"
    map_public_ip_on_launch = true
	Name = "Public-Subnet-1"
	OwnerTag = "${var.OwnerTag}"
}


	
module "public-subnet-b" {
    source = "/Modules/subnet-module"

		vpc_id = "${module.vpc.id}"
		cidr_block = "10.0.4.0/23"
		availability_zone = "${var.region}b"
		map_public_ip_on_launch = true
		Name = "Public-Subnet-2"
		OwnerTag = "${var.OwnerTag}"
	}
module "public-subnet-c" {
    source = "/Modules/subnet-module"

		vpc_id = "${module.vpc.id}"
		cidr_block = "10.0.6.0/23"
		availability_zone = "${var.region}c"
		map_public_ip_on_launch = true
		Name = "Public-Subnet-3"
		OwnerTag = "${var.OwnerTag}"
	}
module "private-subnet-a" {
    source = "/Modules/subnet-module"

		vpc_id = "${module.vpc.id}"
		cidr_block = "10.0.2.0/23"
		availability_zone = "${var.region}a"
		map_public_ip_on_launch = true
		Name = "Private-Subnet-a"
		OwnerTag = "${var.OwnerTag}"
	} 
module "private-subnet-b" {
    source = "/Modules/subnet-module"

		vpc_id = "${module.vpc.id}"
		cidr_block = "10.0.8.0/23"
		availability_zone = "${var.region}b"
		map_public_ip_on_launch = true
		Name = "Private-Subnet-b"
		OwnerTag = "${var.OwnerTag}"
	}   
module "private-subnet-c" {
    source = "/Modules/subnet-module"

		vpc_id = "${module.vpc.id}"
		cidr_block = "10.0.10.0/23"
		availability_zone = "${var.region}c"
		map_public_ip_on_launch = true
		Name = "Private-Subnet-c"
		OwnerTag = "${var.OwnerTag}"
}
module "route" {
	source = "/Modules/route"


}
	

	*/